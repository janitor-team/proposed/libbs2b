libbs2b (3.1.0+dfsg-6) unstable; urgency=medium

  * QA upload.
  * Add pkg-config as BD. Closes: #1020008

 -- Håvard F. Aasen <havard.f.aasen@pfft.no>  Wed, 21 Sep 2022 23:54:59 +0200

libbs2b (3.1.0+dfsg-5) unstable; urgency=medium

  * QA upload.
  * Source-only upload.
  * debian/control: Use "Rules-Requires-Root: no".

 -- Boyuan Yang <byang@debian.org>  Mon, 14 Mar 2022 14:02:57 -0400

libbs2b (3.1.0+dfsg-4) unstable; urgency=medium

  * QA upload.
  * debian/: Bump debhelper compat to v13.
  * debian/control: Apply "wrap-and-sort -abst".
  * Bump Standards-Version to 4.6.0.
  * debian/rules: Enable full hardening.
  * debian/copyright: Refresh information.
  * Add new binary package libbs2b-bin for binary tools.

 -- Boyuan Yang <byang@debian.org>  Sun, 13 Mar 2022 12:10:23 -0400

libbs2b (3.1.0+dfsg-3) unstable; urgency=medium

  * QA upload.
  * Orphan the package via ITS process. (Closes: #1004890)
  * debian/control: Set maintainer to Debian QA Group.
  * debian/control: Use Priority: optional.
  * debian/control: Update Vcs-* fields.

 -- Boyuan Yang <byang@debian.org>  Wed, 23 Feb 2022 20:38:51 -0500

libbs2b (3.1.0+dfsg-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Multiarchify the package. Closes: #770263.
  * Enable parallel builds.
  * Bump standards and debhelper version.

 -- Matthias Klose <doko@ubuntu.com>  Thu, 04 Feb 2016 19:02:31 +0100

libbs2b (3.1.0+dfsg-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use dh-autoreconf to update autotools stuff for new ports (Closes: #702090,
    #727396).
  * Apply patch from Andrew Gainer over at ubuntu to make autoreconf actually
    work.
  * Apply "format security" warning/error fix from Ilya Barygin. I don't think
    it will actually cause a security issue in this context and i'm not sure if
    it will cause a FTBFS in debian (apparently it didn't back in 2011) but it's 
    certainly horrible coding practice to pass a string that is not explicitly
    intended to be a format string to the first parameter of a printf so may
    as well fix it. (Closes: #646327)
  * Remove config.log manually in clean target, it doesn't seem to get removed
    automatically in all situations (in particular it was left behind when
    retrying a build after configure failed).

 -- Peter Michael Green <plugwash@debian.org>  Sat, 04 Oct 2014 11:23:37 +0000

libbs2b (3.1.0+dfsg-2) unstable; urgency=low

  * Update debian/rules so get-orig-source deletes subdirectories of win32/
  * Correct messy entries in copyright
  * Fix bad Vcs-* links

 -- Andrew Gainer <gainer.andrew@gmail.com>  Mon, 25 Jul 2011 13:40:32 -0400

libbs2b (3.1.0+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #634993)

 -- Andrew Gainer <gainer.andrew@gmail.com>  Thu, 21 Jul 2011 00:00:00 -0400
